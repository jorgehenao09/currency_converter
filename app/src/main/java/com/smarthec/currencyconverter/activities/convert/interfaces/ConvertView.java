package com.smarthec.currencyconverter.activities.convert.interfaces;

import com.smarthec.currencyconverter.core.BaseView;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public interface ConvertView extends BaseView{

    /**
     * Muestra errores asociados con el campo valor
     *
     * @param codeMessage
     * @param parameterMessage
     */
    void showErrorMessageValue(String codeMessage, Object[] parameterMessage);

    /**
     * Muestra en pantalla el valor convertido a GBP
     *
     * @param convertValue
     */
    void showValueGBP(Double convertValue);

    /**
     * Muestra en pantalla el valor convertido a EUR
     *
     * @param convertValue
     */
    void showValueEUR(Double convertValue);

    /**
     * Muestra en pantalla el valor convertido a JPY
     *
     * @param convertValue
     */
    void showValueJPY(Double convertValue);

    /**
     * Muestra en pantalla el valor convertido a BRL
     *
     * @param convertValue
     */
    void showValueBRL(Double convertValue);

    /**
     * Retorna el mensaje de error relacionado al consumo del servicio que trae los detalles de la película
     *
     * @return
     */
    String getErrorMessage();
}
