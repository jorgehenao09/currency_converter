package com.smarthec.currencyconverter.activities.details.interfaces;

import com.smarthec.currencyconverter.core.BaseView;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public interface DetailsView extends BaseView{

    /**
     * Modifica el título de la activida
     *
     * @param title
     */
    void setTitle(String title);

    /**
     * Modofica la imagen de la acitividad
     *
     * @param drawable
     */
    void setImage(int drawable);

    /**
     * Muestra la información relacionada a la moneda
     *
     * @param information
     */
    void showInfo(String information);

    /**
     * Modifica la url para mas detalles
     *
     * @param url
     */
    void setURL(String url);
}
