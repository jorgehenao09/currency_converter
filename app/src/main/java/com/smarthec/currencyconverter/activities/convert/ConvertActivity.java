package com.smarthec.currencyconverter.activities.convert;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.smarthec.currencyconverter.R;
import com.smarthec.currencyconverter.activities.about.AboutActivity;
import com.smarthec.currencyconverter.activities.convert.interfaces.ConvertModel;
import com.smarthec.currencyconverter.activities.convert.interfaces.ConvertView;
import com.smarthec.currencyconverter.activities.details.DetailsActivity;
import com.smarthec.currencyconverter.core.BaseActivity;
import com.smarthec.currencyconverter.pojo.CurrencyType;
import com.smarthec.currencyconverter.util.Util;

import butterknife.BindView;

public class ConvertActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, ConvertView {

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.nav_view) NavigationView mNavigationView;

    @BindView(R.id.convert_til_value) TextInputLayout mTilValue;
    @BindView(R.id.convert_tie_value) TextInputEditText mTieValue;

    @BindView(R.id.convert_gbp_value) TextView mGbpValue;
    @BindView(R.id.convert_eur_value) TextView mEurValue;
    @BindView(R.id.convert_jpy_value) TextView mJpyValue;
    @BindView(R.id.convert_brl_value) TextView mBrlValue;

    private ConvertPresenter mPresenter;

    @Override
    public void buildPresenter() {
        super.buildPresenter();

        ConvertModel model = new ConvertModelImpl();

        mPresenter = new ConvertPresenter(this, model);
    }

    @Override
    public void initializeToolbar() {
        super.initializeToolbar();

        mToolbar.setTitle(getString(R.string.title_main_menu));
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * @return El layout id que contiene toda la vista
     */
    @Override
    protected int getLayoutId() {
        return R.layout.activity_converter;
    }

    /**
     * Metodo utilizado para limpiar los datos del formulario
     */
    @Override
    public void initializeForm() {
        mTieValue.setText("");
    }

    /**
     * Metodo encargado de limpiar los mensajes de error mostrados en pantalla
     */
    @Override
    public void cleanErrors() {
        mTilValue.setError(null);

        //Se inicializan los valores
        mGbpValue.setText(getString(R.string.cero));
        mEurValue.setText(getString(R.string.cero));
        mJpyValue.setText(getString(R.string.cero));
        mBrlValue.setText(getString(R.string.cero));
    }

    /**
     * Acciones realizadas al momento de dar clic sobre el boton convertir
     *
     * @param view
     */
    public void convertValue(View view){
        String value = "";

        //Se verifica que el valor que este en el campo de texto no se encuetre vacio
        if (!Util.itsEmptyData(mTieValue.getText().toString())){
            value = mTieValue.getText().toString();
        }

        mPresenter.convertValue(value);
    }

    /**
     * Acciones realizadas al momento de dar clic sobre el boton informacion GBP
     *
     * @param view
     */
    public void infoGBP(View view){
        Intent intent = new Intent(ConvertActivity.this, DetailsActivity.class);

        //Se crea el objeto currencyType
        CurrencyType currencyType = new CurrencyType();
        currencyType.setTitle(getString(R.string.title_activity_gbp));
        currencyType.setDrawable(R.drawable.flag_gbp);
        currencyType.setInformation(getString(R.string.information_gbp));
        currencyType.setUrl("https://es.wikipedia.org/wiki/Libra_esterlina");

        intent.putExtra(DetailsActivity.PARAMETER_TYPE, currencyType);
        startActivity(intent);
    }

    /**
     * Acciones realizadas al momento de dar clic sobre el boton informacion EUR
     *
     * @param view
     */
    public void infoEUR(View view){
        Intent intent = new Intent(ConvertActivity.this, DetailsActivity.class);

        //Se crea el objeto currencyType
        CurrencyType currencyType = new CurrencyType();
        currencyType.setTitle(getString(R.string.title_activity_eur));
        currencyType.setDrawable(R.drawable.flag_eur);
        currencyType.setInformation(getString(R.string.information_eur));
        currencyType.setUrl("https://es.wikipedia.org/wiki/Euro");

        intent.putExtra(DetailsActivity.PARAMETER_TYPE, currencyType);
        startActivity(intent);
    }

    /**
     * Acciones realizadas al momento de dar clic sobre el boton informacion JPY
     *
     * @param view
     */
    public void infoJPY(View view){
        Intent intent = new Intent(ConvertActivity.this, DetailsActivity.class);

        //Se crea el objeto currencyType
        CurrencyType currencyType = new CurrencyType();
        currencyType.setTitle(getString(R.string.title_activity_jpy));
        currencyType.setDrawable(R.drawable.flag_jpy);
        currencyType.setInformation(getString(R.string.information_jpy));
        currencyType.setUrl("https://es.wikipedia.org/wiki/Yen");

        intent.putExtra(DetailsActivity.PARAMETER_TYPE, currencyType);
        startActivity(intent);
    }

    /**
     * Acciones realizadas al momento de dar clic sobre el boton informacion BRL
     *
     * @param view
     */
    public void infoBRL(View view){
        Intent intent = new Intent(ConvertActivity.this, DetailsActivity.class);

        //Se crea el objeto currencyType
        CurrencyType currencyType = new CurrencyType();
        currencyType.setTitle(getString(R.string.title_activity_brl));
        currencyType.setDrawable(R.drawable.flag_brl);
        currencyType.setInformation(getString(R.string.information_brl));
        currencyType.setUrl("https://es.wikipedia.org/wiki/Real_brasileño");

        intent.putExtra(DetailsActivity.PARAMETER_TYPE, currencyType);
        startActivity(intent);
    }

    /**
     * Acciones al dar clic en el boton atrás
     */
    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            closeApplication();
        }
    }

    /**
     * Acciones al dar clic en las opciones del menú lateral
     *
     * @param item
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_info) {
            //Se muestra la pantalla de información
            Intent intent = new Intent(ConvertActivity.this, AboutActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_exit) {
            //Se muestra el dialogo de confirmación
            closeApplication();
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Acciones realizadas al momento de dar clic en la opcion cerrar sesion
     *
     */
    private void closeApplication() {

        //Se muestra una ventana de dialogo
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    //Acciones realizadas al momento de dar clic en SI
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        break;

                    //Acciones realizadas al momento de dar clic en NO
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        //Se construye la ventana de dialogo
        AlertDialog.Builder builder = new AlertDialog.Builder(ConvertActivity.this, R.style.CurrencyAlertDialogCustom);
        builder.setTitle(R.string.title_close).setMessage(R.string.information_warning_logout).setPositiveButton(R.string.btn_yes, dialogClickListener)
                .setNegativeButton(R.string.btn_no, dialogClickListener).show();
    }

    /**
     * Muestra errores asociados con el campo valor
     *
     * @param codeMessage
     * @param parameterMessage
     */
    @Override
    public void showErrorMessageValue(String codeMessage, Object[] parameterMessage) {
        mTilValue.setError(getFormattedMessage(codeMessage, parameterMessage));
    }

    /**
     * Muestra en pantalla el valor convertido a GBP
     *
     * @param convertValue
     */
    @Override
    public void showValueGBP(Double convertValue) {
        mGbpValue.setText(String.valueOf(convertValue));
    }

    /**
     * Muestra en pantalla el valor convertido a EUR
     *
     * @param convertValue
     */
    @Override
    public void showValueEUR(Double convertValue) {
        mEurValue.setText(String.valueOf(convertValue));
    }

    /**
     * Muestra en pantalla el valor convertido a JPY
     *
     * @param convertValue
     */
    @Override
    public void showValueJPY(Double convertValue) {
        mJpyValue.setText(String.valueOf(convertValue));
    }

    /**
     * Muestra en pantalla el valor convertido a BRL
     *
     * @param convertValue
     */
    @Override
    public void showValueBRL(Double convertValue) {
        mBrlValue.setText(String.valueOf(convertValue));
    }

    /**
     * Retorna el mensaje de error relacionado al consumo del servicio que trae los detalles de la película
     *
     * @return
     */
    @Override
    public String getErrorMessage() {
        return getString(R.string.information_error_currency);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
