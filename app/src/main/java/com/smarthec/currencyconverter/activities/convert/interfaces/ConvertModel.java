package com.smarthec.currencyconverter.activities.convert.interfaces;

import com.smarthec.currencyconverter.services.CallbackService;
import com.smarthec.currencyconverter.services.FixerResponse;
import com.smarthec.currencyconverter.util.ResponseProcess;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public interface ConvertModel {

    /**
     * Define los pasos antes de llamar al servicio que permite la conversion
     *
     * @param value
     * @return
     */
    ResponseProcess validateValue(String value);

    /**
     * Realiza el llamado al servvicio que trae los valores de las monedas con respecto al dolar
     *
     * @param callbackService
     * @param value
     */
    void getCurrency(CallbackService<FixerResponse> callbackService, String value);

    /**
     * Realiza los cálculos para necesarios para el dolar en otra moneda
     *
     * @param value
     * @param valueCurrency
     */
    Double getConvertValue(String value, Double valueCurrency);
}
