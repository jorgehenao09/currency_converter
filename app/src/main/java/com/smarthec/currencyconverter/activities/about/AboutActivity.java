package com.smarthec.currencyconverter.activities.about;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.smarthec.currencyconverter.R;
import com.smarthec.currencyconverter.constants.Constants;
import com.smarthec.currencyconverter.core.BaseActivity;
import com.smarthec.currencyconverter.util.Util;

import butterknife.BindView;

public class AboutActivity extends BaseActivity {

    @BindView(R.id.about_tv_version) TextView mNameVersion;
    @BindView(R.id.activity_about) ScrollView mScrollView;
    @BindView(R.id.about_ll_content_email) LinearLayout mContentEmail;
    @BindView(R.id.about_ll_content_bitbucket) LinearLayout mContentBitbucket;

    @Override
    public void initializeView() {
        super.initializeView();
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_about_card_show);
        mScrollView.startAnimation(animation);

        //Se crea la animación
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.setStartOffset(600);

        //Se obtiene y se modifica el valor de la version de la aplicación
        mNameVersion.setText(Util.getVersionName(this));
        mNameVersion.startAnimation(alphaAnimation);

        //Acciones realizadas al momento de dar clic sobre el correo electronico
        mContentEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse(Constants.EMAIL));
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.about_email_intent));
                //intent.putExtra(Intent.EXTRA_TEXT, "Hi,");
                try {
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(AboutActivity.this, getString(R.string.information_not_found_email), Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Acciones realizadas al momento de dar clic sobre el correo electronico
        mContentBitbucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setData(Uri.parse(Constants.BITBUCKET));
                intent.setAction(Intent.ACTION_VIEW);
                startActivity(intent);
            }
        });

    }

    @Override
    public void initializeToolbar() {
        super.initializeToolbar();

        //this line shows back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_about;
    }
}
