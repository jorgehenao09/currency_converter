package com.smarthec.currencyconverter.activities.details;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.smarthec.currencyconverter.R;
import com.smarthec.currencyconverter.activities.details.interfaces.DetailsView;
import com.smarthec.currencyconverter.core.BaseActivity;
import com.smarthec.currencyconverter.pojo.CurrencyType;

import butterknife.BindView;

public class DetailsActivity extends BaseActivity implements DetailsView {

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.app_bar) AppBarLayout mAppBarLayout;

    @BindView(R.id.details_information) TextView mInformation;
    @BindView(R.id.details_more) TextView mMore;

    public static final String PARAMETER_TYPE = "PARAMETER_TYPE";

    private CurrencyType mCurrencyType;
    private DetailsPresenter mPresenter;
    private String mUrl;

    @Override
    public void buildPresenter() {
        super.buildPresenter();

        mPresenter = new DetailsPresenter(this);
    }

    @Override
    public void initializeView() {
        super.initializeView();

        //Se obtiene la información enviada
        mCurrencyType = getIntent().getParcelableExtra(PARAMETER_TYPE);

        mPresenter.initialize(mCurrencyType);

        //Acciones realizadas al momento de dar clic sobre more
        mMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(mUrl));
                startActivity(intent);
            }
        });
    }

    @Override
    public void initializeToolbar() {
        super.initializeToolbar();

        setSupportActionBar(mToolbar);

        //this line shows back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * @return El layout id que contiene toda la vista
     */
    @Override
    protected int getLayoutId() {
        return R.layout.activity_details;
    }

    @Override
    public void initializeForm() {
        //En esta actividad este metodo no se implementa
    }

    @Override
    public void cleanErrors() {
        //En esta actividad este metodo no se implementa
    }

    /**
     * Modifica el título de la activida
     *
     * @param title
     */
    @Override
    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    /**
     * Modofica la imagen de la acitividad
     *
     * @param drawable
     */
    @Override
    public void setImage(int drawable) {
        mAppBarLayout.setBackground(getDrawable(drawable));
    }

    /**
     * Muestra la información relacionada a la moneda
     *
     * @param information
     */
    @Override
    public void showInfo(String information) {
        mInformation.setText(information);
    }

    /**
     * Modifica la url para mas detalles
     *
     * @param url
     */
    @Override
    public void setURL(String url) {
        mUrl = url;
    }
}
