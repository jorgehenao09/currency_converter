package com.smarthec.currencyconverter.activities.details;

import com.smarthec.currencyconverter.activities.details.interfaces.DetailsView;
import com.smarthec.currencyconverter.pojo.CurrencyType;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public class DetailsPresenter {

    private DetailsView mView;

    public DetailsPresenter(DetailsView view) {
        this.mView = view;
    }

    /**
     * Inicializa la pantalla con la informacion correspondiente al tipo de moneda
     *
     * @param currencyType
     */
    public void initialize(CurrencyType currencyType) {
        //modifica el título de la actividad
        mView.setTitle(currencyType.getTitle());
        //Se modifica la imagen del AppBarLayout
        mView.setImage(currencyType.getDrawable());
        //Se muestra información relacionada a la moneda
        mView.showInfo(currencyType.getInformation());
        //Se modifica la url para mostrar mas información relacionada a la moneda
        mView.setURL(currencyType.getUrl());
    }
}
