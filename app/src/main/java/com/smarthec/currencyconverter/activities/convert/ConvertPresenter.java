package com.smarthec.currencyconverter.activities.convert;

import com.smarthec.currencyconverter.activities.convert.interfaces.ConvertModel;
import com.smarthec.currencyconverter.activities.convert.interfaces.ConvertView;
import com.smarthec.currencyconverter.constants.Constants;
import com.smarthec.currencyconverter.services.CallbackService;
import com.smarthec.currencyconverter.services.FixerResponse;
import com.smarthec.currencyconverter.util.EnumResponseType;
import com.smarthec.currencyconverter.util.ReplyMessage;
import com.smarthec.currencyconverter.util.ResponseProcess;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public class ConvertPresenter {

    private ConvertView mView;
    private ConvertModel mModel;

    private CompositeDisposable mDisposeBag;

    public ConvertPresenter(ConvertView view, ConvertModel model) {
        this.mView = view;
        this.mModel = model;

        mDisposeBag = new CompositeDisposable();
    }

    /**
     * Ordena al interactor realizar los pasos necesarios para convertir el valor ingresado por el usuario
     *
     * @param value
     */
    public void convertValue(final String value) {
        //Se muestra un mensaje de espera mientras el proceso se realiza
        mView.showWaitMessage();

        //Se limpia el formulario de posibles errores anteriores
        mView.cleanErrors();

        ResponseProcess response = mModel.validateValue(value);

        //Si la respuesta contiene mensajes de error se procesan
        if(response.containsErrorMessages()) {
            //Se recorren cada uno de los mensajes de la respuesta
            for (ReplyMessage mr : response.getMessages()) {
                //Se procesan solo los mensajes de error
                if (EnumResponseType.ERROR.equals(mr.getResponseType())) {

                    if (Constants.VALUE.equals(mr.getOriginMessage())){
                        mView.showErrorMessageValue(mr.getCodeMessage(), mr.getParameterMessage());
                    }
                }
            }

            //Se esconde el mensaje de espera
            mView.hideWaitMessage();

        } else {

            //llamado asíncrono con Retrofit y RxJava
            mModel.getCurrency(new CallbackService<FixerResponse>() {
                @Override
                public void onSubscribe(Disposable disposable) {
                    mDisposeBag.add(disposable);
                }

                @Override
                public void onResponse(FixerResponse result) {

                    //Se muestra el valor de GBP
                    mView.showValueGBP(mModel.getConvertValue(value, result.getRates().getGBP()));

                    //Se muestra el valor de EUR
                    mView.showValueEUR(mModel.getConvertValue(value, result.getRates().getEUR()));

                    //Se muestra el valor de JPY
                    mView.showValueJPY(mModel.getConvertValue(value, result.getRates().getJPY()));

                    //Se muestra el valor de BRL
                    mView.showValueBRL(mModel.getConvertValue(value, result.getRates().getBRL()));

                    //Se esconde el mensaje de espera
                    mView.hideWaitMessage();
                }

                @Override
                public void onFailure(Throwable error) {
                    //Se esconde el mensaje de espera
                    mView.hideWaitMessage();

                    mView.showMessageInformation(mView.getErrorMessage());
                }
            }, value);
        }
    }

    /**
     * Libera las instacias del view, model y limpiar el Disposable
     */
    public void onDestroy() {
        this.mView = null;
        this.mModel = null;

        //prevents memory leaks by disposing pending observable objects
        if (mDisposeBag != null && !mDisposeBag.isDisposed()) {
            mDisposeBag.clear();
        }
    }
}
