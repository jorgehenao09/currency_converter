package com.smarthec.currencyconverter.activities.splash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.smarthec.currencyconverter.R;
import com.smarthec.currencyconverter.activities.convert.ConvertActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //se construye el hilo
        Thread timerThread = new Thread(){
            @Override
            public void run(){
                try{
                    //Duracion en milisegundos en el que se muestra el splash
                    sleep(2000);
                }catch(InterruptedException e){
                    Log.e("logisticapp", "SplashActivity", e);
                }finally{

                    //Se muestra la pantalla menú principal
                    Intent intent = new Intent(SplashActivity.this, ConvertActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        timerThread.start();
    }
}
