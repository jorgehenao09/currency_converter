package com.smarthec.currencyconverter.activities.convert;

import com.smarthec.currencyconverter.activities.convert.interfaces.ConvertModel;
import com.smarthec.currencyconverter.constants.Constants;
import com.smarthec.currencyconverter.services.CallbackService;
import com.smarthec.currencyconverter.services.FixerResponse;
import com.smarthec.currencyconverter.services.FixerService;
import com.smarthec.currencyconverter.util.ResponseProcess;
import com.smarthec.currencyconverter.util.Util;

import java.text.DecimalFormat;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public class ConvertModelImpl implements ConvertModel{

    private FixerService mService;

    /**
     * Define los pasos antes de llamar al servicio que permite la conversion
     *
     * @param value
     * @return
     */
    @Override
    public ResponseProcess validateValue(String value) {
        ResponseProcess response = new ResponseProcess();

        //Se verifica que el campo fotografia no este vacio
        if (Util.itsEmptyData(value)){
            response.addErrorMessage(Constants.VALUE, Constants.REQUIRED_VALUE);
        } else if (Util.convertToDouble(value, 0) == Constants.CERO ){
            response.addErrorMessage(Constants.VALUE, Constants.INVALID_VALUE);
        }

        return response;
    }

    /**
     * Realiza el llamado al servvicio que trae los valores de las monedas con respecto al dolar
     *
     * @param callbackService
     * @param value
     */
    @Override
    public void getCurrency(final CallbackService<FixerResponse> callbackService, String value) {

        mService = Util.getClient().create(FixerService.class);

        //Se construye el observable
        Observable<FixerResponse> observable = mService.getCurrency()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Function<Throwable, ObservableSource<? extends FixerResponse>>() {
                    @Override
                    public ObservableSource<? extends FixerResponse> apply(Throwable throwable) throws Exception {
                        return io.reactivex.Observable.error(throwable);
                    }
                });

        observable.subscribe(new Observer<FixerResponse>() {

            @Override
            public void onSubscribe(Disposable disposable) {
                //Respuesta al presenter
                callbackService.onSubscribe(disposable);
            }

            @Override
            public void onNext(FixerResponse movieDetailResponse) {
                //Respuesta al presenter
                callbackService.onResponse(movieDetailResponse);
            }

            @Override
            public void onError(Throwable error) {
                //Respuesta al presenter
                callbackService.onFailure(error);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    /**
     * Realiza los cálculos para necesarios para el dolar en otra moneda
     *
     * @param value
     * @param valueCurrency
     */
    @Override
    public Double getConvertValue(String value, Double valueCurrency) {
        DecimalFormat df = new DecimalFormat("#.##");

        //Se retorna el cálculo de la moneda
        return Double.valueOf(df.format(Util.convertToDouble(value, Constants.MAX_DECIMALS) * valueCurrency));
    }
}
