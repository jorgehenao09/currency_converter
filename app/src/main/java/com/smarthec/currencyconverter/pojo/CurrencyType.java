package com.smarthec.currencyconverter.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public class CurrencyType  implements Parcelable{

    private String title;
    private int drawable;
    private String information;
    private String url;

    public CurrencyType() {
    }

    protected CurrencyType(Parcel in) {
        title = in.readString();
        drawable = in.readInt();
        information = in.readString();
        url = in.readString();
    }

    public static final Creator<CurrencyType> CREATOR = new Creator<CurrencyType>() {
        @Override
        public CurrencyType createFromParcel(Parcel in) {
            return new CurrencyType(in);
        }

        @Override
        public CurrencyType[] newArray(int size) {
            return new CurrencyType[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeInt(drawable);
        parcel.writeString(information);
        parcel.writeString(url);
    }
}
