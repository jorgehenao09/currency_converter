package com.smarthec.currencyconverter.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public class ResponseProcess {

    /**
     * Lista de mensaje de respuesta de un proceso
     */
    private List<ReplyMessage> messages;

    /**
     * Constructor por defecto
     */
    public ResponseProcess(){
        messages = new ArrayList<>();
    }

    /**
     * Construye una respuesta de proceso con un unico mensaje
     *
     * @param responseType
     * @param originMessage
     * @param codeMessage
     * @param parametersMessage
     */
    public ResponseProcess(EnumResponseType responseType, String originMessage, String codeMessage, Object... parametersMessage){
        this();
        messages.add(new ReplyMessage(responseType, originMessage, codeMessage, parametersMessage));
    }

    /**
     * Construye una respuesta de proceso con un unico mensaje
     *
     * @param responseType
     * @param originMessage
     * @param parametersMessage
     */
    public ResponseProcess(EnumResponseType responseType, String originMessage, Object... parametersMessage){
        this();
        messages.add(new ReplyMessage(responseType, originMessage, parametersMessage));
    }

    /**
     * Agrega un mensaje de error a la respuesta de un proceso
     *
     * @param originMessage
     * @param codeMessage
     * @param parametersMessage
     */
    public void addErrorMessage(String originMessage, String codeMessage, Object... parametersMessage){
        messages.add(new ReplyMessage(EnumResponseType.ERROR, originMessage, codeMessage, parametersMessage));
    }

    /**
     * Agrega un mensaje de advertencia a la respuesta de un proceso
     *
     * @param originMessage
     * @param codeMessage
     * @param parametersMessage
     */
    public void addWarningMessage(String originMessage, String codeMessage, Object... parametersMessage){
        messages.add(new ReplyMessage(EnumResponseType.WARNING, originMessage, codeMessage, parametersMessage));
    }

    /**
     * Agrega un mensaje de error a la respuesta de un proceso
     *
     * @param codeMessage
     * @param parametersMessage
     */
    public void addErrorMessage(String codeMessage, Object... parametersMessage){
        addErrorMessage(codeMessage, null, parametersMessage);
    }

    /**
     * Agrega un mensaje de advertencia a la respuesta de un proceso
     *
     * @param codeMessage
     * @param parametersMessage
     */
    public void addWarningMessage(String codeMessage, Object... parametersMessage){
        addWarningMessage(codeMessage, null, parametersMessage);
    }

    /**
     * Verifica si en los mensajes de respuesta existe al menos uno de tipo ERROR
     * @return
     */
    public Boolean containsErrorMessages(){
        for(ReplyMessage mr : messages){
            if(EnumResponseType.ERROR.equals(mr.getResponseType())){
                return true;
            }
        }

        return false;
    }

    /**
     * Verifica si en los mensajes de respuesta existe al menos uno de tipo ADVERTENCIA
     * @return
     */
    public Boolean containsWarningMessages(){
        for(ReplyMessage mr : messages){
            if(EnumResponseType.WARNING.equals(mr.getResponseType())){
                return true;
            }
        }

        return false;
    }

    public List<ReplyMessage> getMessages() {
        return messages;
    }
}
