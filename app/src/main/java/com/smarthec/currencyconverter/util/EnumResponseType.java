package com.smarthec.currencyconverter.util;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public enum EnumResponseType {

    WARNING,
    ERROR
}
