package com.smarthec.currencyconverter.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.smarthec.currencyconverter.R;
import com.smarthec.currencyconverter.constants.Constants;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public class Util {

    private static Retrofit retrofit = null;

    /**
     * Verifica si una cadena esta vacia
     *
     * @param cadena
     * @return
     */
    public static boolean itsEmptyData(String cadena) {
        if (cadena == null || cadena.length() == 0) {
            return true;
        }

        String cadenaAux = cadena.trim();

        if (cadenaAux.length() == 0) {
            return true;
        }

        return false;
    }

    /**
     * Construye el formateador de numeros decimales
     * <br/><br/>
     * Separador de miles es la coma (,), el separador decimal es punto (.)
     *
     * @return
     */
    public static DecimalFormat obtenerFormateadorNumeros(int minimoDecimales, int maximoDecimales) {

        //Se definen los separadores validos para numeros
        DecimalFormatSymbols separadores = new DecimalFormatSymbols();
        separadores.setDecimalSeparator('.');
        separadores.setGroupingSeparator(',');

        DecimalFormat df = new DecimalFormat();
        //Se le definen al decimal format los simbolos validos
        df.setDecimalFormatSymbols(separadores);
        df.setGroupingSize(3);

        //Minimo y maximo de decimales
        df.setMinimumFractionDigits(minimoDecimales);
        df.setMaximumFractionDigits(maximoDecimales);

        return df;
    }

    /**
     * Convierte una cadena a un Double
     *
     * @param valor
     * @return
     */
    public static Double convertToDouble(String valor, int maximoDecimales) {
        if (itsEmptyData(valor)) {
            return null;
        }

        Double resultado = 0D;

        DecimalFormat formateadorNumeros = Util.obtenerFormateadorNumeros(2, maximoDecimales);

        try {
            resultado = formateadorNumeros.parse(valor).doubleValue();
        } catch (ParseException e) {
            Log.e(Constants.UTIL,"Error haciendo conversion del numero " + valor, e);
        }

        return resultado;
    }

    /**
     * Retorna la instancia de retrofit
     *
     * @return
     */
    public static Retrofit getClient() {

        if (retrofit == null) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.URL_MAIN)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(buildClient())
                    .build();
        }

        return retrofit;
    }

    /**
     * Metodo utilizado para construir un objeto OkHttpClient
     *
     * @return
     */
    public static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    /**
     * Retorna la versión de la aplicación
     *
     * @param context
     * @return
     */
    public static String getVersionName(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return context.getString(R.string.version) + " " + version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
