package com.smarthec.currencyconverter.constants;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public interface Constants {

    //Base de la Url
    String URL_MAIN = "http://api.fixer.io/";

    String UTIL = "UTIL";
    String VALUE = "VALUE";

    int MAX_DECIMALS = 2;

    //Mensajes de error
    String REQUIRED_VALUE = "requiredValue";
    String INVALID_VALUE = "invalidValue";
    Double CERO = 0D;
    String EMAIL = "mailto:joehenaoro@gmail.com";
    String BITBUCKET = "https://bitbucket.org/jorgehenao09/currency_converter";
}
