package com.smarthec.currencyconverter.services;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class FixerResponse {
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("rates")
  @Expose
  private Rates rates;
  @SerializedName("base")
  @Expose
  private String base;
  public void setDate(String date){
   this.date=date;
  }
  public String getDate(){
   return date;
  }
  public void setRates(Rates rates){
   this.rates=rates;
  }
  public Rates getRates(){
   return rates;
  }
  public void setBase(String base){
   this.base=base;
  }
  public String getBase(){
   return base;
  }
}