package com.smarthec.currencyconverter.services;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Jorge Henao on 5/03/2018.
 */

public interface FixerService {

    /**
     * Obtiene el listado para la conversion
     *
     * @return
     */
    @GET("latest?base=USD")
    Observable<FixerResponse> getCurrency();
}
