package com.smarthec.currencyconverter.services;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Rates{
  @SerializedName("CHF")
  @Expose
  private Double CHF;
  @SerializedName("HRK")
  @Expose
  private Double HRK;
  @SerializedName("MXN")
  @Expose
  private Double MXN;
  @SerializedName("ZAR")
  @Expose
  private Double ZAR;
  @SerializedName("INR")
  @Expose
  private Double INR;
  @SerializedName("CNY")
  @Expose
  private Double CNY;
  @SerializedName("THB")
  @Expose
  private Double THB;
  @SerializedName("AUD")
  @Expose
  private Double AUD;
  @SerializedName("ILS")
  @Expose
  private Double ILS;
  @SerializedName("KRW")
  @Expose
  private Double KRW;
  @SerializedName("JPY")
  @Expose
  private Double JPY;
  @SerializedName("PLN")
  @Expose
  private Double PLN;
  @SerializedName("GBP")
  @Expose
  private Double GBP;
  @SerializedName("IDR")
  @Expose
  private Double IDR;
  @SerializedName("HUF")
  @Expose
  private Double HUF;
  @SerializedName("PHP")
  @Expose
  private Double PHP;
  @SerializedName("TRY")
  @Expose
  private Double TRY;
  @SerializedName("RUB")
  @Expose
  private Double RUB;
  @SerializedName("HKD")
  @Expose
  private Double HKD;
  @SerializedName("ISK")
  @Expose
  private Double ISK;
  @SerializedName("EUR")
  @Expose
  private Double EUR;
  @SerializedName("DKK")
  @Expose
  private Double DKK;
  @SerializedName("CAD")
  @Expose
  private Double CAD;
  @SerializedName("MYR")
  @Expose
  private Double MYR;
  @SerializedName("BGN")
  @Expose
  private Double BGN;
  @SerializedName("NOK")
  @Expose
  private Double NOK;
  @SerializedName("RON")
  @Expose
  private Double RON;
  @SerializedName("SGD")
  @Expose
  private Double SGD;
  @SerializedName("CZK")
  @Expose
  private Double CZK;
  @SerializedName("SEK")
  @Expose
  private Double SEK;
  @SerializedName("NZD")
  @Expose
  private Double NZD;
  @SerializedName("BRL")
  @Expose
  private Double BRL;

    public Double getCHF() {
        return CHF;
    }

    public void setCHF(Double CHF) {
        this.CHF = CHF;
    }

    public Double getHRK() {
        return HRK;
    }

    public void setHRK(Double HRK) {
        this.HRK = HRK;
    }

    public Double getMXN() {
        return MXN;
    }

    public void setMXN(Double MXN) {
        this.MXN = MXN;
    }

    public Double getZAR() {
        return ZAR;
    }

    public void setZAR(Double ZAR) {
        this.ZAR = ZAR;
    }

    public Double getINR() {
        return INR;
    }

    public void setINR(Double INR) {
        this.INR = INR;
    }

    public Double getCNY() {
        return CNY;
    }

    public void setCNY(Double CNY) {
        this.CNY = CNY;
    }

    public Double getTHB() {
        return THB;
    }

    public void setTHB(Double THB) {
        this.THB = THB;
    }

    public Double getAUD() {
        return AUD;
    }

    public void setAUD(Double AUD) {
        this.AUD = AUD;
    }

    public Double getILS() {
        return ILS;
    }

    public void setILS(Double ILS) {
        this.ILS = ILS;
    }

    public Double getKRW() {
        return KRW;
    }

    public void setKRW(Double KRW) {
        this.KRW = KRW;
    }

    public Double getJPY() {
        return JPY;
    }

    public void setJPY(Double JPY) {
        this.JPY = JPY;
    }

    public Double getPLN() {
        return PLN;
    }

    public void setPLN(Double PLN) {
        this.PLN = PLN;
    }

    public Double getGBP() {
        return GBP;
    }

    public void setGBP(Double GBP) {
        this.GBP = GBP;
    }

    public Double getIDR() {
        return IDR;
    }

    public void setIDR(Double IDR) {
        this.IDR = IDR;
    }

    public Double getHUF() {
        return HUF;
    }

    public void setHUF(Double HUF) {
        this.HUF = HUF;
    }

    public Double getPHP() {
        return PHP;
    }

    public void setPHP(Double PHP) {
        this.PHP = PHP;
    }

    public Double getTRY() {
        return TRY;
    }

    public void setTRY(Double TRY) {
        this.TRY = TRY;
    }

    public Double getRUB() {
        return RUB;
    }

    public void setRUB(Double RUB) {
        this.RUB = RUB;
    }

    public Double getHKD() {
        return HKD;
    }

    public void setHKD(Double HKD) {
        this.HKD = HKD;
    }

    public Double getISK() {
        return ISK;
    }

    public void setISK(Double ISK) {
        this.ISK = ISK;
    }

    public Double getEUR() {
        return EUR;
    }

    public void setEUR(Double EUR) {
        this.EUR = EUR;
    }

    public Double getDKK() {
        return DKK;
    }

    public void setDKK(Double DKK) {
        this.DKK = DKK;
    }

    public Double getCAD() {
        return CAD;
    }

    public void setCAD(Double CAD) {
        this.CAD = CAD;
    }

    public Double getMYR() {
        return MYR;
    }

    public void setMYR(Double MYR) {
        this.MYR = MYR;
    }

    public Double getBGN() {
        return BGN;
    }

    public void setBGN(Double BGN) {
        this.BGN = BGN;
    }

    public Double getNOK() {
        return NOK;
    }

    public void setNOK(Double NOK) {
        this.NOK = NOK;
    }

    public Double getRON() {
        return RON;
    }

    public void setRON(Double RON) {
        this.RON = RON;
    }

    public Double getSGD() {
        return SGD;
    }

    public void setSGD(Double SGD) {
        this.SGD = SGD;
    }

    public Double getCZK() {
        return CZK;
    }

    public void setCZK(Double CZK) {
        this.CZK = CZK;
    }

    public Double getSEK() {
        return SEK;
    }

    public void setSEK(Double SEK) {
        this.SEK = SEK;
    }

    public Double getNZD() {
        return NZD;
    }

    public void setNZD(Double NZD) {
        this.NZD = NZD;
    }

    public Double getBRL() {
        return BRL;
    }

    public void setBRL(Double BRL) {
        this.BRL = BRL;
    }
}