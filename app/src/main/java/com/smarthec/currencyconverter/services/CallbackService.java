package com.smarthec.currencyconverter.services;

import io.reactivex.disposables.Disposable;

/**
 * Interfaz para definir las acciones a realizar despues de obtener respuesta de un llamado al servicio
 *
 * Created by Jorge Henao on 5/03/2018.
 */

public interface CallbackService<T> {

    /**
     * Accion a realizar cuando se obtiene el disposable
     *
     * @param disposable
     */
    void onSubscribe(Disposable disposable);

    /**
     * Accion a realizar cuando se obtuvo una respuesta del servidor
     *
     * @param result
     */
    void onResponse(T result);

    /**
     * Accion a realizar cuando ha ocurrido un error
     *
     * @param error
     */
    void onFailure(Throwable error);
}
